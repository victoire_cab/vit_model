from google.colab import drive
drive.mount('/content/drive')

#### path to the storage location of the samples that will be downloaded

os.chdir('/content/')

#### Drebin malicious samples download link path and download
!wget -q -nc --show-progress --user drebin --password h9FN43vH --no-check-certificate https://www.sec.cs.tu-bs.de/~danarp/drebin/dataset/drebin-5.zip
!wget -q -nc --show-progress --user drebin --password h9FN43vH --no-check-certificate https://www.sec.cs.tu-bs.de/~danarp/drebin/dataset/drebin-4.zip
!wget -q -nc --show-progress --user drebin --password h9FN43vH --no-check-certificate https://www.sec.cs.tu-bs.de/~danarp/drebin/dataset/drebin-2%C2%BD.zip
!wget -q -nc --show-progress --user drebin --password h9FN43vH --no-check-certificate https://www.sec.cs.tu-bs.de/~danarp/drebin/dataset/drebin-33%E2%85%93.zip
!wget -q -nc --show-progress --user drebin --password h9FN43vH --no-check-certificate https://www.sec.cs.tu-bs.de/~danarp/drebin/dataset/drebin-1.zip
!wget -q -nc --show-progress --user drebin --password h9FN43vH --no-check-certificate https://www.sec.cs.tu-bs.de/~danarp/drebin/dataset/drebin-0.zip
!wget https://github.com/Yaweni/Vic/releases/download/Data/ops.txt
!wget -q --show-progress  https://bitbucket.org/JesusFreke/smali/downloads/baksmali-2.5.2.jar

!unzip -P infected -n  '*drebin*.zip'

os.chdir('/content/')
f=glob.glob('*-*/*')
!rm -r /content/extracted*
!mkdir extracted


for apk in f:

  dir=os.path.basename(apk)
  try:
    zip_ref=zipfile.ZipFile(apk, 'r') 
  except:
    print("Pas ficher zip")
  else:
    os.chdir('/content/extracted/')
    try:
        zip_ref.extractall(dir)
    except:
      print('Error')
  finally:
    os.chdir('/content/')

 os.chdir('/content/')


!mkdir smali

#### take into account that the first 1050 apk


NUM_PROC = 2

os.chdir('/content/extracted')
b=glob.glob('*')
def func(i,t,j):
    #Perform one action here
    a=(j/t).__floor__()
    if i != (t-1):
      cut = b[i*a:(i+1)*a]
    else:
      cut = b[t*a:]
      print(len(cut))
    for dir in tqdm(cut):
      path=dir+'/*.dex'
      dest= '/content/smali/'+ dir
      !java -jar /content/baksmali-2.5.2.jar disassemble $path -o $dest
for dir in tqdm(b[:1050]):
    path=dir+'/*.dex'
    dest= '/content/smali/'+ dir
    os.chdir('/content/smali')
    if dir not in glob.glob('*'):
      os.chdir('/content/extracted')
      !java -jar /content/baksmali-2.5.2.jar disassemble $path -o $dest
    os.chdir('/content/extracted')


def std_codes_list(standard_codes):

    std_codes = [] #empty list which will later contain all the standard op-codes read from the ops.txt file

    with open(standard_codes,'r') as fp:
        for cnt, line in enumerate(fp): # reading each op-code in the txt file
            print(cnt,line)
            read_lines = fp.read()
            read_lines = read_lines.split("\n")
            std_codes = read_lines
    return std_codes

#### the ops.txt file contains the official list of Android opcodes

standard_codes = "/content/ops.txt" 


app_dir= "/content/smali/"


def parse_smalis(app_dir,each_apk):

    smali_content = [] #Empty list that will later contain all of the content of all of the smali files.
    

    # import ipdb
    # ipdb.set_trace() 
    files = glob.glob(app_dir+each_apk+"/**/*.smali", recursive = True)
    #for root, dirs, files in os.walk(app_dir+each_apk+"/smali"): #Scanning through each file in each sub-directory
    for file in files:
            

        #if file.endswith(".smali"):

            

        #file_dest= os.path.join(root, file)
        file_dest=file
        with open(file_dest,'r') as fp:
            smali_content += fp.readlines()
    smali_content = [line.rstrip('\n').split(" ") for line in smali_content] #store the contents of a file

    return smali_content

def match_op_codes(std_codes, smali_content,each_apk):

    occ = ""
    for each_line in smali_content:
        for each_word in each_line:
            if (each_word) in (std_codes):
                occ+=each_word+" "
    fq= {}
    
    fq.setdefault('Apk Name',each_apk)
    fq.setdefault('Codes',occ)
    fq.setdefault('Label','Malware')
    return fq

std_codes = std_codes_list(standard_codes)
os.chdir('/content/smali')

apk_list = glob.glob('*')
valeurs=[]
for each_apk in apk_list:
    smali_content = parse_smalis(app_dir,each_apk)
    fq = match_op_codes(std_codes,smali_content,each_apk)
    valeurs.append(fq)

apk_info = ['Apk Name', 'Codes','Label']
os.chdir('/content/')

with open('Drebin.csv', 'w') as csvfile:
    writer = csv.DictWriter(csvfile, fieldnames = apk_info)
    writer.writeheader()
    writer.writerows(valeurs)

df.head()


df=pd.read_csv("Drebin.csv")
table = pa.Table.from_pandas(df)
os.chdir('/content/drive/MyDrive')
pq.write_table(table, 'Drebin1050.parquet', compression='BROTLI')

df.shape

