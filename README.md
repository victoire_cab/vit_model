What does this repository contain?

    This repo contains a python implementation of transformer model for malware analysis 
What package/platform dependencies do I need to have to run the code?

    The following packages need to be installed to run the code:
    1. numpy
    2. matplotlib.pyplot
    3. pandas
    4. torch
    5. sklearn
    6. transformers

How do I use the different files?

1.Drebin_Traitement.ipynb
 ce fichier permet de:
    • Télécharger le dataset des échantillons malveillant (Drebin dataset).
    • Désassembler les apk a l’aide d’apktool et backsmali.
    • D’extraire les opcodes des differents apk en ce basant sur les opcodes officiels contenus dans le fichier ops.txt.
    • De retourner la sequence d’opcode extraite sous la forme (nom_apk , Codes , Labels).
    • De stocker le resultat de l’extraction dans un fichier.csv (Drebin.csv).
    • De parquetiser  le fichier.csv et de l'enregistrer sous le nom Drebin.parquet.

2. opcodes_manuel.ipynb
 
 ce fichier permet de:
    • Lire les fichiers parquetiser(benin\malware) precedemment sauvegarder 
    • Définir  le nombre échantillons a utiliser pour l' entraînement du modèle.
    • Tokeniser les séquences d’opcodes  
    • De sauvegarder le résultat de la tokenisation dans le fichier file.pkl et les différents labels dans le fichier label.pkl
    • De générer les images de tailles 3080*3080 et de les stocker dans les fichiers Begnin et Malware .

3. Vit_Model.ipynb

C’est le fichier principal d’ entrainement du modèle . Il permet de 
    • Définir le chemin d’accès aux dossiers contenant les images générer dans le fichier opcodes_manuel.py
    • Repartir les données 
    • d’afficher le résultat du modèle 


 
