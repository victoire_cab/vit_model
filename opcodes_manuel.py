

import pandas as pd
# cd drive/MyDrive/opcodes_dataset/Parquet

import requests

url = "http://159.223.12.156/casual-server/cabrelle/"
response = requests.get(url)
if response.status_code == 200:
    print(response.content)
else:
    print("Erreur lors de la récupération du fichier.")

# prendre 500 echantions par dossiers benins et  malware(Drebin1050.parquet)

df=pd.concat([pd.read_parquet("1b"),
               pd.read_parquet("2b"),
               pd.read_parquet("3.0b"),
               pd.read_parquet("3.1b"),
               pd.read_parquet("3.2b"),
               pd.read_parquet("3.3b"),
               pd.read_parquet("3.4b"),
               pd.read_parquet("n1.0"),
               pd.read_parquet("n2.0"),
               pd.read_parquet("n3.0"),
               pd.read_parquet("n4.0"),
               pd.read_parquet("n5.0"),
               pd.read_parquet("n6.0"),
               pd.read_parquet("n8.0"),
               pd.read_parquet("n9.0"),
               pd.read_parquet("n99.0"),
               pd.read_parquet("n11.0"),
               pd.read_parquet("Drebin1050.parquet"),])
df=df.drop('Apk Name', axis=1)
df = df.dropna()
df = df[df['Codes'].map(len) > 50]
df = df.groupby('Label').sample(n=1000, random_state=1)
df = df.sample(frac=1, random_state=1).reset_index(drop=True)

std_codes = [] #empty list which will later contain all the standard op-codes read from the ops.txt file

with open('ops.txt','r') as fp:
    for cnt, line in enumerate(fp): # reading each op-code in the txt file
        read_lines = fp.read()
        read_lines = read_lines.split("\n")
        std_codes = read_lines

# tokenisation

import numpy as np
from tqdm import tqdm

vals=df["Codes"]
data=[]
lvals=vals.values.tolist()
for val in tqdm(lvals):
  z=val.split(" ")
  interim=[]
  for code in z:
    interim.append(std_codes.index(code))
  data.append(interim)


import gc
gc.collect()


y=set()
for i in range (256):
  y.add(i)


x=set()
tst=y-set(data[0])
x=tst.intersection(*[y-set(code) for code in data])

lents=[]
for code in data:
  code_set=set(code)
  lents.append(len(code_set))
max(lents)

data_padded=[]
pad=len(max(data, key=len))
for i in data:
  i+=[58]*(pad-len(i))
  data_padded.append(i)

len(data_padded[0])

import pickle

# le fichier file.pkl contiendra le resulat de la tokenisation

with open('file.pkl', 'wb') as file:
    pickle.dump(data_padded, file)

with open('label.pkl', 'wb') as file:
    pickle.dump(df['Label'].values.tolist(), file)

# sauvegarde des fichiers file.pkl et label.pkl 

file=open("file.pkl","rb")
data_padded=pickle.load(file)
file.close()

file=open("label.pkl","rb")
labels=pickle.load(file)
file.close()

import numpy as np
from PIL import Image as im
from tqdm import tqdm

import os

# creation des fichier benign et Malware qui contiendra les images d'apk benin et malveillant respectivement

os.mkdir('./Benign')
os.mkdir('./Malware')

from matplotlib import pyplot as plt

# generation des images et sauvegarde

taille=4000*4000
pads=data_padded
for i,label,number in tqdm(zip(pads,labels,range(len(pads)))):
  print(len(i))
  d=i+([58]*(taille-len(i)))
  print(len(d))
  array=np.asarray(d,dtype=np.uint8)
  array=np.reshape(array,(4000,4000))
  array = array.astype('int8')
  #plt.imshow(array, cmap='gray')
  #plt.colorbar()
  #plt.tight_layout()
  #plt.show()
  data = im.fromarray(np.uint8(array*255))
  name=str(number+1)+".png"
  if label == 'Benign':
    os.chdir('Benign')
    data.save(name)
    os.chdir("..")
  else:
    os.chdir('Malware')
    data.save(name)
    os.chdir('..')

taille=4000*4000
pads=data_padded
for i,label,number in tqdm(zip(pads[4:6],labels[4:6],range(len(pads[4:6])))):
  print(len(i))
  d=i+([0]*(taille-len(i)))
  print(len(d))

  array=np.asarray(d,dtype=np.uint8)
  print(array.dtype)
  array=np.reshape(array,(4000,4000))
  print(array.dtype)
